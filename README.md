
# Project Title: SweetCherryTech Challenge

## Overview
This repository contains two main projects for the SweetCherryTech Challenge. The `challenge` directory hosts the back-end application developed in Java 11 with Spring Boot, and the `challenge-front-end` directory contains the front-end application built with React. Additionally, there is a SQL dump file `sweetcherrytech_challenge_db.sql` for setting up the MySQL database required for the back-end.

## Getting Started

### Prerequisites
- Java 11
- Node.js and npm
- MySQL Server

### Setting Up the Database
1. Start your MySQL server.
2. Create a new MySQL database for the project.
3. Import the SQL dump file to your database:
   ```
   mysql -u username -p database_name < sweetcherrytech_challenge_db.sql
   ```
   Replace `username` with your MySQL username and `database_name` with the name of the database you created.

### Running the Back-End (Spring Boot)
Navigate to the `challenge` directory:
```
cd challenge
```
Run the application:
```
./mvnw spring-boot:run
```
The back-end server should start and connect to the MySQL database.

### Running the Front-End (React)
Navigate to the `challenge-front-end` directory:
```
cd challenge-front-end
```
Install the necessary packages:
```
npm install
```
Start the React application:
```
npm start
```
The front-end should now be accessible in your web browser.
