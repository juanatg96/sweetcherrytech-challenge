package com.sweetcherrytech.challenge.controllers;

import com.sweetcherrytech.challenge.models.Producto;
import com.sweetcherrytech.challenge.services.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/productos")
public class ProductoController {

    private final ProductoService productoService;

    @Autowired
    public ProductoController(ProductoService productoService) {
        this.productoService = productoService;
    }

    @GetMapping
    public ResponseEntity<Page<Producto>> searchProductos(
            @RequestParam(required = false) String search,
            Pageable pageable) {
        if (search == null) {
            return ResponseEntity.ok(productoService.findAll(pageable));
        } else {
            return ResponseEntity.ok(productoService.search(search, pageable));
        }
    }
}
