package com.sweetcherrytech.challenge.services;

import com.sweetcherrytech.challenge.models.Producto;
import com.sweetcherrytech.challenge.repositories.ProductoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

@Service
public class ProductoService {

    private final ProductoRepository productoRepository;

    @Autowired
    public ProductoService(ProductoRepository productoRepository) {
        this.productoRepository = productoRepository;
    }

    public Page<Producto> findAll(Pageable pageable) {
        return productoRepository.findAll(pageable);
    }

    public Page<Producto> search(String search, Pageable pageable) {
        return productoRepository.findByTituloContainingOrPrecio(search, pageable);
    }
}
