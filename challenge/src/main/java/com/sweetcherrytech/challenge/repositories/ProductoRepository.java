package com.sweetcherrytech.challenge.repositories;


import com.sweetcherrytech.challenge.models.Producto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;

@Repository
public interface ProductoRepository extends JpaRepository<Producto, Integer> {
    @Query("SELECT p FROM Producto p WHERE " +
            "LOWER(p.titulo) LIKE LOWER(CONCAT('%', :search, '%')) OR " +
            "p.precio LIKE CONCAT('%', :search, '%')")
    Page<Producto> findByTituloContainingOrPrecio(String search, Pageable pageable);
}
