import { useCallback, useState, useEffect } from 'react';
import Head from 'next/head';
import { Box, Container, Stack, Typography } from '@mui/material';
import { Layout as DashboardLayout } from 'src/layouts/dashboard/layout';
import { ProductosTable } from 'src/sections/productos/productos-table';
import { ProductosSearch } from 'src/sections/productos/productos-search';

const Page = ({ initialData, initialPage, initialRowsPerPage }) => {
  const [page, setPage] = useState(initialPage);
  const [rowsPerPage, setRowsPerPage] = useState(initialRowsPerPage);
  const [data, setData] = useState(initialData);
  const [search,setSearch] = useState("");

  useEffect(() => {
    const fetchData = async () => {
      const response = await fetch(`http://localhost:8080/challenge/api/productos?search=${search}&page=${page}&size=${rowsPerPage}`);
      const newData = await response.json();
      setData(newData);
    };

    fetchData();
  }, [page, rowsPerPage, search]);

  const handlePageChange = useCallback(
    (event, value) => {
      setPage(value);
    },
    []
  );

  const handleRowsPerPageChange = useCallback(
    (event) => {
      setRowsPerPage(event.target.value);
    },
    []
  );

  const handleSearch = (searchTerm) => {
    setSearch(searchTerm);
    setPage(0);
  };

  return (
    <>
      <Head>
        <title>Productos | Devias Kit</title>
      </Head>
      <Box component="main" sx={{ flexGrow: 1, py: 8 }}>
        <Container maxWidth="xl">
          <Stack spacing={3}>
            <Stack direction="row" justifyContent="space-between" spacing={4}>
              <Stack spacing={1}>
                <Typography variant="h4">Productos</Typography>
              </Stack>
            </Stack>
            <ProductosSearch onSearch={handleSearch} />
            <ProductosTable
              count={data.totalElements} 
              items={data.content} 
              onPageChange={handlePageChange}
              onRowsPerPageChange={handleRowsPerPageChange}
              page={page}
              rowsPerPage={rowsPerPage}
            />
          </Stack>
        </Container>
      </Box>
    </>
  );
};

export async function getServerSideProps(context) {
  const page = context.query.page || 0;
  const rowsPerPage = context.query.rowsPerPage || 5;

  const res = await fetch(`http://localhost:8080/challenge/api/productos?page=${page}&size=${rowsPerPage}`);
  const initialData = await res.json();

  return {
    props: {
      initialData,
      initialPage: parseInt(page, 10),
      initialRowsPerPage: parseInt(rowsPerPage, 10),
    },
  };
}

Page.getLayout = (page) => <DashboardLayout>{page}</DashboardLayout>;

export default Page;
