import { useEffect, useState } from 'react';
import MagnifyingGlassIcon from '@heroicons/react/24/solid/MagnifyingGlassIcon';
import { Card, InputAdornment, OutlinedInput, SvgIcon } from '@mui/material';

export const ProductosSearch = ({ onSearch }) => {
  const [searchTerm, setSearchTerm] = useState('');

  useEffect(() => {
    if (isValidSearchTerm(searchTerm)) {
      onSearch(searchTerm);
    }
  }, [searchTerm]);

  const isValidSearchTerm = (term) => {
    // Adjust the regex as per your requirements
    const regex = /^[a-zA-Z0-9\s.,]*$/;
    return (term.length >= 0 || term === '') && regex.test(term);
  };

  const handleInputChange = (event) => {
    const inputTerm = event.target.value;
    if (isValidSearchTerm(inputTerm)) {
      setSearchTerm(inputTerm);
    }
  };

  return (
    <Card sx={{ p: 2 }}>
      <OutlinedInput
        value={searchTerm}
        onChange={handleInputChange}
        fullWidth
        placeholder="Buscar producto por Titulo o Precio..."
        startAdornment={(
          <InputAdornment position="start">
            <SvgIcon
              color="action"
              fontSize="small"
            >
              <MagnifyingGlassIcon />
            </SvgIcon>
          </InputAdornment>
        )}
        sx={{ maxWidth: 500 }}
      />
    </Card>
  );
};